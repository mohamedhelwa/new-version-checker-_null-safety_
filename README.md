# New Version Checker (Null-Safety)

A simple steps to Check if a new version is available for Flutter Apps (Null-Safety) on App-store or Google-play store.

## Getting started

1- Import the [new_version](https://pub.dev/packages/new_version/install) package to your **_pubspec.yaml_** file

     new_version: ^0.3.0

2- Add the included **translation keys** to your **assets/lang/ar.json** file.

```
  "updateAvailable": "تحديث متاح",
  "updateNow": "تحديث",
  "later": "لاحقًا",
  "youCanUpdateFrom": " يمكنك الآن تحديث هذا التطبيق من ",
  "to": " إلي ",
```


3- Add the included **translation keys** to your **assets/lang/en.json** file.

```
  "updateAvailable": "Update Available",
  "updateNow": "Update",
  "later": "Maybe Later",
  "youCanUpdateFrom": "You can now update this app from",
  "to": " to ",
```


4- Create a new object from **_NewVersion_** class and add these lines to your **_HomeScreen_**.


```

final newVersion = NewVersion(); 

void _checkVersionUpdateStatus(BuildContext context) async {
    final status = await newVersion.getVersionStatus();

    newVersion.showUpdateDialog(
      context: context,
      versionStatus: status!,
      dialogTitle: tr(context, "updateAvailable"),
      dialogText: "${tr(context, "youCanUpdateFrom")} ${status.localVersion} ${tr(context, "to")} ${status.storeVersion}",
      updateButtonText: tr(context, "updateNow"),
      dismissButtonText: tr(context, "later"),
      allowDismissal: true,
    );
  }
    
```



5- Call the  **_ _checkVersionUpdateStatus(context);_**  method inside **_initState()_** method.


```
_checkVersionUpdateStatus(context);
```
